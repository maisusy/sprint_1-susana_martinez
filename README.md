PROYECTO DELLILAH RESTO
Aministracion de datos de un restaurante

DOMINIO : https://www.acamicadelilahresto.tk

REQUISITOS 📋
SE NECESITAN LOS SIGUIENTES PAQUETES CON LAS SIGUIENTES VERSIONES:
    "axios": "^0.25.0",
    "chai": "^4.3.4",
    "chai-http": "^4.3.0",
    "cors": "^2.8.5",
    "dotenv": "^10.0.0",
    "formidable": "^2.0.1",
    "helmet": "^4.6.0",
    "jsonwebtoken": "^8.5.1",
    "mercadopago": "^1.5.12",
    "mocha": "^9.1.3",
    "moment": "^2.29.1",
    "mongoose": "^6.0.4",
    "node-fetch": "^2.6.1",
    "passport": "^0.5.2",
    "passport-facebook": "^3.0.0",
    "passport-github": "^1.1.0",
    "passport-google-oauth20": "^2.0.0",
    "passport-linkedin-oauth2": "^2.0.0",
    "swagger-ui-express": "^4.1.6",
    "yamljs": "^0.3.0"

 

INSTALACION y EJECUCIÓN🔧
Sigue los siguientes pasos dentro de una terminal:
1. git init (inicializar git)
2. git clone https://gitlab.com/maisusy/sprint_1-susana_martinez.git (clonar el repositorio)
3. cd sprint_1-susana_martinez (moverse a la carpeta creada con el comando )
4. npm i (instalar las dependencias si el sistema lo pide use 'npm audit fix')
5. npm run dev (para ejecutar el programa) 

DOCKER
Dentro de la terminar ejecutar lo siguiente(debe estar dentro de la carpeta /sprint_1-susana_martinez):
Ejecutar:
1. sudo docker build --tag app . (crear la imagen)
2. sudo docker run --name app -p 8080:8080 app (correr la imagen)
3. sudo docker containers ls -a(para ver las imagenes que estan en ejecución)

BASE DE DATOS
Para conectarse a la base datos usara mongocompass el cual debera instalar.Para esto le dejamos la pagina la cual le proporcionara las instrucciones necesarias para su uso.
link:https://docs.mongodb.com/compass/current/install/
Luego de su instalacion ingresara a mongoDBCompass e inserta los siguiente "mongodb+srv://DelilahResto:AcamicaPruebaUsuario431298@sprint-acamica.pyntb.mongodb.net/test?authSource=admin&replicaSet=atlas-lhy2vw-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true" en donde dige "Click edit to modify your connection string (SRV or Standard )"despues solamente click en "Connect" y listo ya tiene la base de datos conectada. 

TEST
Para el testeo debe ingresar en la consola y usa el comando  "npm test"

USO DE POSTMAN
A continuacion se le dejara un video para saber la utilizacion del token en postman.
link:https://youtu.be/7c6w_Ezw_UA
Y para mas informacion debajo dejare una pequña descripcion de lo que hace cada ruta(https://www.acamicadelilahresto.tk/resto):

INICIO
/inicio/login Nos permite iniciar sesion
/inicio/alta Nos permite crear un nuevo usuario
/inicio/CerrarSesion Nos permite cerrar la sesion
PEDIDO
/pedidos/modificar/estado/{num} Nos permite modificar el estado de un pedido
/pedidos/altaPedido Nos permite registrar un pedido
/pedidos/historial Nos mostrara un listado de los pedidos 
PRODUCTO
/productos/ Nos mostrara un listado de los producto 
/productos/alta Nos permite registrar un nuevo producto
/productos/modificar/{nombre} Nos permite modifcar un producto
/productos/baja/{nombre} Nos permite eliminar un producto
USUARIO
/usuarios/ Nos muestra un listado de los usuarios
/usuarios/bajaCuenta  Nos permite eliminar la cuenta con la que estemos logueados en el momento
/usuarios/actualizarCuenta Nos permite actualizar la cuenta con la que estemos logueados en el momento
/usuarios/SuspenderCuenta/{user} Nos permite suspender a un usuario 
/usuarios/CancelarSuspencion/{user} Nos permite anular la suspencion de un usuario
MEDIOS DE PAGO
/medio_de_pago/ Nos muestra un listado de medios de pago
/medio_de_pago/alta Nos permite crear un nuevo medio de pago
/medio_de_pago/modificar/{nombre} Nos permite modificar un medio de pago
/medio_de_pago/baja/{nombre} Nos permite eliminar un medio de pago
AUTH
/google/auth Auth de google
/facebook/auth Auth de facebook
/linkedin/auth Auth de linkedin
/github/auth Auth de github
CALLBACK
/google/auth/callback Callback de google
/facebook/auth/callback Callback de facebook
/linkedin/auth/callback Callback de linkedin
/github/auth/callback Callback de github
PASARELA DE PAGO
/process_payment Pasarela de pago(mercado pago)

SWAGGER
https://www.acamicadelilahresto.tk/swagger

Autor ✒️
-nombre:Susana Martinez
    -correo electronico: maisusy15@outlook.com
    -repositorio:https://gitlab.com/maisusy/sprint_1-susana_martinez.git


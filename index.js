//**************************PAQUETES*************************************/
const express = require("express")
const Helmet = require("helmet")
const app = express()

//**************************CONFIGURACION CORDS*************************************/
const cors = require('cors')
app.use(cors())

//**************************CONFIGURACION EXPRESS*************************************/
app.use(express.urlencoded({extended:true}));
app.use(express.json())
app.use(Helmet())
app.use(express.static(__dirname + '/src/database/db.js'))

//**************************CONFIGURACION SWAGGER*************************************/
const YAML = require("yamljs")
const swaggerJcDoc = YAML.load(__dirname + "/SWAGGER.yaml");
const swaggerUi = require("swagger-ui-express");
app.use('/swagger',swaggerUi.serve,swaggerUi.setup(swaggerJcDoc));

//**************************CONFIGURACION PASSPORT*************************************/
const initServices = require(__dirname + '/src/services');
const passport = require('passport')
const initMercadoPagoRouter = require(__dirname + '/src/router/api/mercadopago')
const prepareRoutes = require(__dirname + '/src/router/auth');
initServices(app);  
app.use(prepareRoutes());
app.use(initMercadoPagoRouter())
app.use(passport.initialize());

//**************************RUTA PRINCIPAL*************************************/
const ApiRuta = require(__dirname + "/src/router/api")
app.use("/resto",ApiRuta)
app.get('/',(req,res)=>{
    res.send({
      msg:"bienvenido"
    })
  } )
  
//***************************ESCUCHANDO EL SERVIDOR*******************************************/
const env = require(__dirname + "/src/config/index")
app.listen(env.PORT,
    ()=>{console.log(`Escuchando servidor en: ${env.DB_HOST}${env.PORT} \nEscuchando swagger en :${env.SWAGGER}`) });

FROM node:16-alpine
COPY ./ /back-end
RUN cd /back-end && npm install && npm audit fix 
CMD cd /back-end && npm run dev

const chai = require('chai');
const assert = require('chai').assert;
const env = require("../src/config")
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
const url= `${env.HOST}${env.PORT}`;

describe("#TEST DE REGISTRO NUEVOS USUARIOS",( ) => {

    it("Crear un nuevo usuario",async() => {
        chai.request(url)
        .post(`/resto/inicio/alta`)
        .send({
            _id: "kb34b1i4b2i43",
            usuario:"testmocha",
            nom_ape:"test",
            correo:"testmocha@test.com",
            telefono:23454235,
            direccion:"test 124",
            contrasenia:"test",
            admin:false,
            logueado:false,
            suspendido : false
        })
        .end((err,res)=>{
            console.log(res.body)
            assert.equal(res.status, 200)
        }).timeout(10000);
    })


})

function VerDetalle(){
    window.location.href = ("../pedidos/detalle-pedido/detalle.html");
}

function Salir(){
    window.location.href = ("https://www.acamicadelilahresto.tk");
}

function Comprar(descripcion,precio){
    window.location.href = ('../mercado_pago/index.html')

}

function Volver(){
    window.location.href = ("../menu/menu-inicio.html");
}

function cambiarestilo(){
    let est = document.getElementById("estado").value;
    console.log(est);
    switch(est){
        case "nuevo":
            document.getElementById("estado").style.backgroundColor = "red"
            document.getElementById("estado").style.color = "white";
            break;
        case "confirmado":
            document.getElementById("estado").style.backgroundColor = "orange"
            document.getElementById("estado").style.color = "white";
            break;
        case "preparando":
            document.getElementById("estado").style.backgroundColor = "yellow"
            document.getElementById("estado").style.color = "black";
            break;
        case "enviando":
            document.getElementById("estado").style.backgroundColor = "greenyellow"
            document.getElementById("estado").style.color = "black";
            break;
        case "cancelado":
            document.getElementById("estado").style.backgroundColor = "lightblue"
            document.getElementById("estado").style.color = "black";
            break;
        case "entregado":
            document.getElementById("estado").style.backgroundColor = "gray"
            document.getElementById("estado").style.color = "white";
            break;
        default:
            break;
    }

}

function aniadir(){
    window.location.href = ("../registro/detalle.html")
}

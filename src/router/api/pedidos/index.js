const router = require("express").Router()
const controller = require("../../../controller/pedido");
const EstadoMiddleware = require("../../../middleware/estados")
const ProductoMiddleware = require("../../../middleware/productos")
const MedioDePagoMiddleware = require("../../../middleware/medios_de_pago")
const PedidoMiddleware = require("../../../middleware/pedidos")
const UsuarioMiddleware = require("../../../middleware/usuarios")


router.post("/altaPedido",
PedidoMiddleware.DatosPedidos,
MedioDePagoMiddleware.ExistenciaFormadePago,
ProductoMiddleware.ExistenciaProducto,
PedidoMiddleware.validarCantidad,
PedidoMiddleware.validarDireccion,
controller.alta
)

 router.get("/historial",
    controller.historial
 );


router.use(UsuarioMiddleware.es_admin)

router.put("/modificar/estado/:num",
PedidoMiddleware.DatosPedidos,
PedidoMiddleware.ExistenciaPedido,
EstadoMiddleware.ExistenciaEstado,
controller.ActualizarEstado
)


module.exports = router;
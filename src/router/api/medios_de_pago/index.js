const router = require("express").Router()
const MedioDePagoMiddleware = require("../../../middleware/medios_de_pago")
const UsuarioMiddleware = require("../../../middleware/usuarios")
const controller = require("../../../controller/medios_de_pago")


router.use(UsuarioMiddleware.es_admin)

router.get(
    "/",
    controller.lista
)


router.delete(
    "/baja/:nombre",
    MedioDePagoMiddleware.DatosFormasDePago, 
    MedioDePagoMiddleware.ExistenciaFormadePago,
    controller.baja
)

router.post(
    "/alta",
    MedioDePagoMiddleware.DatosFormasDePago,
    MedioDePagoMiddleware.ExistenciaFormadePago,
    controller.alta
) 


 router.put(
    "/modificar/:nombre",
    MedioDePagoMiddleware.DatosFormasDePago, 
    MedioDePagoMiddleware.ExistenciaFormadePago,
    controller.actualizar
 )

module.exports = router;
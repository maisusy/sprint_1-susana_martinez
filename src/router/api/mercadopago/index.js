
const express = require('express')
const mercadopago = require('mercadopago');
const env = require('../../../config/index.js');

function initMercadoPagoRouter() {
  const router = express.Router()
  
  router.post('/process_payment', (req, res) => {
    mercadopago.configurations.setAccessToken(env.MERCADOPAGO_ACCESS_TOKEN); 
    mercadopago.payment.save(req.body)
      .then(function (response) {
        const { status, status_detail, id } = response.body;
	console.log("todo ok",status_detail);
        res.status(response.status).json({ status, status_detail, id });
      })
      .catch(function (error) {
        console.error(error);
        res.status(406).json({status:406, message:error})
      })
  })

  return router;
}

module.exports = initMercadoPagoRouter

const router = require("express").Router()
const UsuarioMiddleware = require("../../../middleware/usuarios")
const controller = require("../../../controller/usuario")

router.put(
    "/actualizarCuenta",
    UsuarioMiddleware.DatosUsuario_Login,
    UsuarioMiddleware.Email,
    controller.actualizar
)


router.delete(
    "/bajaCuenta",
    controller.baja
)

router.use(UsuarioMiddleware.es_admin)

router.get(
    "/",
    controller.lista
)

router.put(
    "/SuspenderCuenta/:user",
    UsuarioMiddleware.DatosUsuario_Login,
    UsuarioMiddleware.ExistenciaUsuario,
    controller.suspender
)

router.put(
    "/CancelarSuspencion/:user",
    UsuarioMiddleware.DatosUsuario_Login,
    UsuarioMiddleware.ExistenciaUsuario,
    controller.calcelarsuspencion
)

router.post(
    "/AgregarDireccion",
    controller.AgregarDireccion
)

module.exports = router;

const express = require('express');
const router = express.Router();
const google = require('./google');
const facebook = require('./facebook');
const linkedin = require('./linkedin'); 
const github = require('./github');

function prepareRoutes() {
  router.use(facebook());
  router.use(google());
  router.use(linkedin());
  router.use(github());
  return router
}

module.exports = prepareRoutes;

const express = require('express');

const passport = require('passport');


function prepareRouter() {
  const router = express.Router();

  const strategy_name = 'linkedin';

  router.get('/linkedin/auth', passport.authenticate(strategy_name,
))

  router.get('/linkedin/auth/callback' , passport.authenticate(strategy_name, 
   {
      failureRedirect: 'https://www.acamicadelilahresto.tk/',
      session: false,
    }),
    function (req, res) {
      res.redirect(`https://www.acamicadelilahresto.tk/menu/menu-inicio.html?token=aca_va_el_token`);
    }
);


  return router;
}

module.exports = prepareRouter;

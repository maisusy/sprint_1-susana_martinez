const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const strategy_name = 'google';
  const router = express.Router();
  
  router.get('/google/auth', passport.authenticate(strategy_name, {
    scope: ['profile', 'email'],
    session: false,
  }));


  router.get('/google/auth/callback', passport.authenticate(strategy_name, 
   {
      failureRedirect: 'https://www.acamicadelilahresto.tk/',
      session: false,
    }),
    function (req, res) {
      res.redirect(`https://www.acamicadelilahresto.tk/menu/menu-inicio.html?token=aca_va_el_token`);
    }
  );
  return router;
}

module.exports = prepareRouter;

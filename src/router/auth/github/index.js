const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const strategy_name = 'github';
  const router = express.Router();
  
  router.get('/github/auth', passport.authenticate(strategy_name))

  router.get('/github/auth/callback', passport.authenticate(strategy_name,
   {
      failureRedirect: 'https://www.acamicadelilahresto.tk/',
      session: false,
    }),
    function (req, res) {
      res.redirect(`https://www.acamicadelilahresto.tk/menu/menu-inicio.html`)
    }
  );
  return router;
}

module.exports = prepareRouter;


const mongoose = require("mongoose");
const Env = require("../config")

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

const path = `mongodb://${Env.DB_USER}:${Env.DB_PASS}@sprint-acamica-shard-00-00.pyntb.mongodb.net:27017,sprint-acamica-shard-00-01.pyntb.mongodb.net:27017,sprint-acamica-shard-00-02.pyntb.mongodb.net:27017/${Env.DB_DATABASE}?ssl=true&replicaSet=atlas-lhy2vw-shard-0&authSource=admin&retryWrites=true&w=majority`;

mongoose.connect(path);

module.exports = mongoose;
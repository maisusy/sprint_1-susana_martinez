const {Schema} = require("../database/db");

const usu = new Schema({
    usuario : String,
    nom_ape : String,
    correo : String,
    telefono : Number,
    direccion : Array,
    contrasenia : String,
    admin : Boolean,
    logueado : Boolean,
    suspendido : Boolean,
})

exports.usuario = usu

const {Schema} = require("../database/db");
const Detalle = require("./estado")

const Pedido = new Schema({
    num : Number,
    estado : {
        _id : String,
        nom : String
    },
    hora : String,
    descripcion : String,
    detalle_productos : [Detalle],
    forma_de_pago : {
        _id : String,
        nom : String
    },
    total : Number,
    usuario : {
        usuario : String,
        nom_ape : String,
        correo : String,
        telefono : Number,
        direccion : String,
        contrasenia : String
    },
    direccion : String
});

exports.Pedido = Pedido;
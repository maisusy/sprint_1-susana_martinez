const Usuarios = require("../controller/usuario").Usuarios
const jwt = require("jsonwebtoken")
const firma = "token"

exports.login = async (req,res)=>{
      try{
        await Usuarios.findOne({
            usuario : req.body.usuario,
            contrasenia : req.body.contrasenia
        })
        .then( result => {
            if(result != undefined){
                result.logueado = true;
                result.save()
                const token = jwt.sign({
                    usuario : result.usuario,
                    nom_ape : result.nom_ape,
                    correo : result.correo,
                    telefono : result.telefono,
                    direccion : result.direccion,
                    contrasenia : result.contrasenia,
                    admin : result.admin,
                    logueado : true,
                    suspendido : result.suspendido
                },firma,{expiresIn:"1h"})

                res.status(200).json({
                    msg : "USUARIO logueado",
                    token : token
                })
            }else{
                res.status(404).json({
                    msg : "La contrasenia no coincide con el usaurio"
                })
            }
        })
    }catch(error){
        res.status(404).json({
            msg : "HA OCURRIDO UN ERROR",
            error : error
        })
    }
    
}

exports.alta = async (req,res)=>{

    const datos = new Usuarios({
                    usuario:req.body.usuario,
                    nom_ape:req.body.nom_ape,
                    correo:req.body.correo,
                    telefono:parseInt(req.body.telefono),
                    contrasenia:req.body.contrasenia,
                    admin:false,
                    logueado:false,
                    suspendido : false
                })
    datos.direccion.push(req.body.direccion)
    await datos.save()
    .then( result =>{
        res.status(200).json(
            {
                msg:"USUARIO CREADO",
                datos:result
            }
        )
    })
    .catch( err =>{
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL CREAR EL USUARIO",
                error:err
            }
        )
    })
}

exports.cerrarSesion = async(req,res,next) =>{
    try{
        await Usuarios.findOne({logueado : true})
        .then( result => {
            if( result != undefined){
                result.logueado = false;
                result.save()
                res.status(200).json({
                    msg : "HA CERRADO SESION CORRECTAMENTE"
                })
            }else{
                res.status(404).json({
                    msg : "USTED NO ESTA LOGUEADO PARA CERRAR UNA SESION"
                })
            }
        })
    }catch(err){
        res.status(505).json({
            msg : "HA OCURRIDO UN ERROR AL CERRAR SESION",
            error : err
        })
    }

}



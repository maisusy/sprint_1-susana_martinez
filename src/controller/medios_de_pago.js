const Schema = require("../models/medio_de_pago").MedioDePago
const con = require("../database/db")
const MedioDePago = con.model("MediosDePagos",Schema)

exports.MedioDePago = MedioDePago


exports.lista = async(req,res) => {
    await MedioDePago.find()
    .then( result => {
        if(result.length > 1){
            res.status(200).json(
                {
                    msg:"Listado de medios de pago",
                    datos:result
                }
            )
        }else{
            res.status(404).json(
                {
                    msg:"No se encontraron medios de pago"
                }
            )
        }
        
    })
    .catch( err => {
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL LISTAR LOS MEDIOS DE PAGO",
                datos:err
            }
        )
    })
}

exports.alta =  async (req,res)=>{
    
    const datos = new MedioDePago({
        nom: req.body.nom
    })
    datos.save()
    .then( result =>{
        res.status(200).json(
            {
                msg:"medio de pago creado",
                datos:result
            }
        )
    })
    .catch( err =>{
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL CREAR EL MEDIO DE PAGO",
                error:err
            }
        )
    })

}

exports.baja = async(req,res)=>{
    await MedioDePago.deleteOne({nom : req.params.nombre})
    .then( result =>{
        res.status(200).json(
            {
                msg:"medio de pago eliminado",
                datos:result
            }
        )
    })
    .catch( err =>{
        res.status(404).json(
            {
                msg:"HA OCURRIDO UN ERROR AL ELIMINAR EL MEDIO DE PAGO",
                error:err
            }
        )
    })

}

exports.actualizar = async(req,res)=>{
    try{
        await MedioDePago.findOne({nom : req.params.nombre})
        .then( result =>{
            result.nom = req.body.nom
            result.save()
            res.status(200).json(
                {
                    msg:"medio de pago actualizado",
                    datos:result
                }
            )
        })
    }catch(err){
        res.status(404).json({
            msg : "HA OCURRIDO UN ERROR AL ACTUALIZAR EL MEDIO DE PAGO",
            error : err
        })
    }

}

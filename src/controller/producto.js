const Schema = require("../models/producto").producto
const con = require("../database/db")
const Productos = con.model("Productos",Schema)
const env = require("../config/index")


exports.Productos = Productos

exports.listado = async(req,res)=>{

   try{
        const prod = await Productos.find()

        return res.status(200).json({
                    msg:"Listado de productos",
                    datos:prod
                })
    }catch(err){
        return res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL LISTAR LOS PRODUCTOS",
            error:err
        })
    }
        
};

exports.alta = ( async (req,res) => {

    const datos = new Productos ({
        nombre:req.body.nombre,
        precio:parseInt(req.body.precio)
    });

    await datos.save()

    .then( result => {
        res.status(200).json({
            msg:"Creacion de producto",
            datos: result
        })
    })
    .catch(err => {
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL CREAR EL PRODUCTO",
            error:err
        })
    })

});

exports.actualizar = ( async (req,res) => {

    try{
        await Productos.findOne({nombre : req.params.nombre})
        .then( result => {
            result.nombre = req.body.nombre
            result.precio = req.body.precio
            result.save()
            res.status(200).json({
                msg:"Actualizacion del producto",
                datos: result
            })
        })
    }catch(err){
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL ACTUALIZAR EL PRODUCTO",
            error:err
        })
    }
    
})

exports.eliminar = ( async (req,res) => {

    await Productos.deleteOne({nombre : req.params.nombre})
    .then( result => {
        res.status(200).json({
            msg:"Elimacion del producto",
            datos: result
        })
    })
    .catch(err => {

        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL ELIMINAR EL PRODUCTO",
            error:err
        })

    })

})

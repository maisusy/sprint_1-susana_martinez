const MedioDePago = require("../controller/medios_de_pago").MedioDePago

async function ExistenciaFormadePago(req,res,next){
    switch(req.url){
        case "/altaPedido":
            try{
                await  MedioDePago.findOne({nom : req.body.forma_de_pago})
                .then(async(result) => {
                    if(result == undefined){
                        const formaspago = await MedioDePago.find()
                        res.status(404).json({
                            msg : `La forma de pago no existe.Intente con alguna de estas:`,
                            datos : formaspago
                        })
                    }else{
                        next()
                    }
                })
            }catch(err){
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DE MEDIO DE PAGO",
                    error : err
                })
            }
            break;
        case "/alta":
            await  MedioDePago.findOne({nom : req.body.nom})
            .then(result => {
                if(result == undefined){
                    next()
                }else{
                    res.status(404).json({
                        msg : `La forma de pago ${req.body.nom} ya existe`,
                    })
                }
            })
            .catch(err => {
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DEL MEDIO DE PAGO",
                    error : err
                })
            })
            break;
        case `/modificar/${req.params.nombre.replaceAll(" ","%20")}`:
            try{
                await  MedioDePago.findOne({nom : req.params.nombre})
                .then(async (result) => {
                    if(result != undefined){
                        await  MedioDePago.findOne({nom : req.body.nom})
                        .then(resultado => {
                            if(resultado == undefined){
                                next()
                            }else{
                                res.status(404).json({
                                    msg : `La forma de pago ${req.body.nom} ya existe`,
                                })
                            }
                        })
                    }else{
                        res.status(404).json({
                            msg : `La forma de pago buscada ${req.params.nombre} no existe`,
                        })
                    }
                })
            }catch(err){
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAR LA EXISTENCIA DEL MEDIO DE PAGO",
                    error : err
                })
            }
            break;
        case `/baja/${req.params.nombre.replaceAll(" ","%20")}`:
            await  MedioDePago.findOne({nom : req.params.nombre})
            .then(result => {
                if(result != undefined){
                    next()
                }else{
                    res.status(404).json({
                        msg : `La forma de pago buscada ${req.params.nombre} no existe`,
                    })
                }
            })
            .catch(err => {
                res.status(404).json({
                    msg : "HA OCURRIDO UN ERROR AL VALIDAD LA EXISTENCIA DEL MEDIO DE PAGO",
                    error : err
                })
            })
            break;
        }

}

async function DatosFormasDePago(req,res,next){
   switch(req.url){
    case "/alta":
        if(!req.body.nom){
            res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
        }else{
            if( req.body.nom!=="" ){
                next()
            }else{
                res.status(404).json({ msg :"LOS CAMPOS NO DEBEN ESTAR VACIOS"})
            }
         }   
         break;
    case `/modificar/${req.params.nombre.replaceAll(" ","%20")}`:
        if(!req.body.nom || !req.params.nombre){
            res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
        }else{
            if( req.body.nom!=="" || req.params.nombre!=="" ){
                next()
            }else{
                res.status(404).json({ msg :"LOS CAMPOS NO DEBEN ESTAR VACIOS"})
            }
        }   
        break;
    case `/baja/${req.params.nombre.replaceAll(" ","%20")}`:
        if(!req.params.nombre){
            res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
        }else{
            if(req.params.nombre!=="" ){
                next()
            }else{
                res.status(404).json({ msg :"LOS CAMPOS NO DEBEN ESTAR VACIOS"})
            }
        }   
        break;
    
    }

}

module.exports = {
    ExistenciaFormadePago,
    DatosFormasDePago
}


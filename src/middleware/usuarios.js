const Usuarios = require("../controller/usuario").Usuarios
const jwt = require("jsonwebtoken")
const firma = "token"

async function esta_logueado(req,res,next){

    const token = req.body.token  
    if(token.logueado == true){
            next()
    }else{
            res.status(404).json({
                msg : `NO SE HA logueado`,
            })
        }
}

async function es_admin(req,res,next){

    const token = req.body.token  
    if(token.admin == true){
         next()
    }else{
         res.status(404).json({
             msg : `TIENE QUE SER ADMIN PARA ACCEDER AQUI`,
         })
     }

}

async function DatosUsuario_Login(req,res,next){
    switch(req.url){
        case "/alta" :
            if(!req.body.usuario || !req.body.nom_ape || !req.body.correo || !req.body.telefono || !req.body.direccion || !req.body.contrasenia ) {
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.body.usuario=="" ||req.body.nom_ape=="" || req.body.correo=="" || req.body.telefono=="" || req.body.direccion=="" || req.body.contrasenia=="" ) {
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }else{
                    next()
                }
            }
            break;
        case `/actualizarCuenta` :
            if(!req.body.usuario || !req.body.nom_ape || !req.body.correo || !req.body.telefono || !req.body.direccion || !req.body.contrasenia ) {
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.body.usuario=="" ||req.body.nom_ape=="" || req.body.correo=="" || req.body.telefono=="" || req.body.direccion=="" || req.body.contrasenia=="" ) {
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }else{
                    next()
                }
            }
            break;
        case "/login":
            if(!req.body.usuario  || !req.body.contrasenia ) {
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.body.usuario=="" || req.body.contrasenia=="" ) {
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }else{
                    next()
                }
            }

            break;
        default :
            if(!req.params.user ){
                res.status(404).json({ msg : "SE REQUIEREN TODOS LOS CAMPOS"})
            }else{
                if(req.params.user=="" ) {
                    res.status(404).json({ msg : "LOS CAMPOS NO DEBEN ESTAR VACIOS"})
                }else{
                    next()
                }
            }
            break;
    }
}

async function ExistenciaUsuario(req,res,next){    
    switch(req.url){
        case `/SuspenderCuenta/${req.params.user}` :
            await Usuarios.findOne({usuario : req.params.user})
            .then( result => {
                if(result != undefined){
                    next()
                }else{
                    res.status(404).json({msg:`NO EXISTE EL USUARIO '${req.params.user}' `})
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR LA EXISTENCIA DEL USUARIO",
                    error : err})
            })
            break;
        case `/CancelarSuspencion/${req.params.user}` :
            await Usuarios.findOne({usuario : req.params.user})
            .then( result => {
                if(result != undefined){
                    next()
                }else{
                    res.status(404).json({msg:`NO EXISTE EL USUARIO '${req.params.user}' `})
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR LA EXISTENCIA DEL USUARIO",
                    error : err})
            })
            break;
        case "/alta":
            await Usuarios.findOne({usuario : req.body.usuario})
            .then( result => {
                if(result != undefined){
                    res.status(404).json({msg:`YA EXISTE EL USUARIO '${req.body.usuario}' `})

                }else{
                    next()
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR LA EXISTENCIA DEL USUARIO",
                    error : err})
            })
            break;
        case "/login" :
            await Usuarios.findOne({usuario : req.body.usuario})
            .then( result => {
                if(result != undefined || result != null){
                    next()
                }else{
                    res.status(404).json({msg:`NO EXISTE EL USUARIO '${req.body.usuario}' `})
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR LA EXISTENCIA DEL USUARIO",
                    error : err})
            })
            break;
        default :
            await Usuarios.findOne({usuario : req.body.usuario})
            .then( result => {
                if(result != undefined){
                    next()
                }else{
                    res.status(404).json({msg:`NO EXISTE EL USUARIO '${req.body.usuario}' `})
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR LA EXISTENCIA DEL USUARIO",
                    error : err})
            })
            break;
    }
}

async function Email(req,res,next){
    const{correo}=req.body
    estructura = /\S+@\S+\.\S+/;
    if(estructura.test(correo) === true){     

        await Usuarios.findOne({correo : correo})
        .then( result => {
            if(result != undefined){
                res.status(404).json({msg:`Ya Existe un usuario con el correo '${correo}'`})
            }else{
                next()
            }
        })
    }else{
        res.status(404).json({msg:"EL FORMATO DEL EMAIL NO ES VALIDO"})
    }
}

async function deslogeo(req,res,next) {
    await Usuarios.find({logueado : true})
    .then(result => {
        if(result.length < 1){
            next()
        }else{
            res.status(404).json({msg:"DEBE CERRAR LA SESION PARA ABRIR UNA NUEVA"})
        }
    })
    .catch((err) => {
        res.status(404).json({
            msg:"HA OCURRIDO UN ERROR AL VERIFICAR QUE NO ESTE logueado ANTERIORMENTE",
            error:err
        })
    })
}

function AunteticacionToken(req,res,next) {
    try{
        const token = req.headers.authorization
        const verificarToken = jwt.verify(token,firma)
        if(verificarToken){
            req.body.token = verificarToken;
            return next()
        }
    }catch(e){
        res.status(404).json({
            msg:"Debe tener un token",
            error:e
        })
    }
        
}

async function suspencion(req,res,next) {
    await Usuarios.findOne({usuario : req.body.usuario})
            .then( result => {
                if(result.suspendido == false){
                    next()
                }else{
                    res.status(404).json({msg:`EL USUARIO '${req.body.usuario}' ESTA SUSPENDIDO`})
                }
            })
            .catch(err => {
                res.status(404).json({ 
                    msg:"HA OCURRIO UN ERROR AL VERIFICAR SI EL USUARIO ESTABA SUSPENDIDO",
                    error : err})
            })
}


module.exports = {
    esta_logueado,
    es_admin,
    DatosUsuario_Login,
    Email,
    deslogeo,
    AunteticacionToken,
    ExistenciaUsuario,
    suspencion
}

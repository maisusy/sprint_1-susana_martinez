const passport = require('passport');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
const env = require('../../config/index.js')

function prepareStrategy() {
 
  const strategy_name = 'linkedin';

  passport.use(strategy_name, new LinkedInStrategy({
    clientID: env.LINKEDIN_KEY,
    clientSecret: env.LINKEDIN_SECRET,
    callbackURL: env.LINKEDIN_CALLBACK,
   scope: ['r_emailaddress', 'r_liteprofile'],
    },
    function (accessToken, refreshToken, profile, done) {
      if(profile==undefined){ 
        return done(err,profile);
       }else{
        return done(null,profile);
        }
   }
  ));
}

module.exports = prepareStrategy;


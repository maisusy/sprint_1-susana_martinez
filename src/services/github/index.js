const passport = require('passport');
const GitHubStrategy  = require('passport-github').Strategy;
const env = require('../../config/index.js')

function prepareStrategy() {

  passport.use("github",new GitHubStrategy({
      clientID: env.GITHUB_CLIENT_ID,
      clientSecret: env.GITHUB_CLIENT_SECRET,
      callbackURL: env.GITHUB_CALLBACK
    },
    function(accessToken, refreshToken, profile, cb){
     if(profile==undefined){ 
        return cb(err,profile);
       }else{
        return cb(null,profile);
        }
    }
  ));

}

module.exports = prepareStrategy;


const passport = require('passport');
const passportFacebookStrategy = require('passport-facebook');
const env = require('../../config')

function prepareStrategy() {
  const FacebookStrategy = passportFacebookStrategy.Strategy;
  const strategy_name = 'facebook';

  passport.use(strategy_name, new FacebookStrategy({
      clientID: env.FACEBOOK_CLIENT_ID,
      clientSecret: env.FACEBOOK_CLIENT_SECRET,
      callbackURL: env.FACEBOOK_CALLBACK,
	 scope: [ 'email']
    },
    function(accessToken, refreshToken, profile,done){
     if(profile==undefined){ 
      return done(err,profile);
     }else{
	    return done(null,profile);
      }
    }
 ));
}

module.exports = prepareStrategy;

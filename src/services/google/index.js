const passport = require('passport');
const passportGoogleStrategy = require('passport-google-oauth20');
const env = require('../../config/index.js')

function prepareStrategy() {
  const GoogleStrategy = passportGoogleStrategy.Strategy;

  const strategy_name = 'google';

  passport.use(strategy_name, new GoogleStrategy({
    clientID: env.GOOGLE_CLIENT_ID,
    clientSecret: env.GOOGLE_CLIENT_SECRET,
    callbackURL: env.GOOGLE_CALLBACK,
  },
    function (accessToken, refreshToken, profile, done) {
      if(profile==undefined){ 
        return done(err,profile);
       }else{
        return done(null,profile);
        }
   }
  ));
}

module.exports = prepareStrategy;

const prepareGoogleStrategy = require('./google');
const prepareFacebookStrategy = require('./facebook');
const prepareLinkedinStrategy = require('./linkedin');
const prepareGithubStrategy = require('./github');

function initialize() {
    prepareGoogleStrategy()
    prepareFacebookStrategy()
   prepareLinkedinStrategy()
    prepareGithubStrategy()
}

module.exports = initialize;
